# Activitats sobre Criptografia 

**Nota: Entrega les activitats en un document de text. Si cal, fes una captura de la sortida de del programa per demostrar el seu funcionament.**

**Nota: Desa les diferents versions dels programes en arxius/classes executables diferents**

1. **Com s’instància un objecte per generar claus? Quines classes ofereix java en funció del tipus d’encriptació que es vol fer anar (simètrica o asimètrica)?**

Les classes generadores de claus son:

- La classe abstracta `java.security.KeyPairGenerator` per a xifrats asimètrics.
- La classe abstracta `javax.crypto.KeyGenerator` per a xifrats simètrics.

Un `KeyGenerator` s'instància fent servir l'expressió `KeyGenerator keygen = KeyGenerator.getInstance([String nom-algorisme]);`
Un`KeyPairGenerator` s'instància fent servir l'expressió `KeyPairGenerator foo = KeyPairGenerator.getInstance([String nom-algorisme])`.

2. **Com s'anomena el patró de programació per generar les claus i altres instàncies com del xifrador?**

És el patró *Fàbrica*, que permet crear un objecte a través d'una interfície (de contracte, d'exposició a l'usuari, no de OOP) comuna, sense coneixer el nom de la classe a la que pertany.

![fac-pattern]

Com tots els patrons de programació, deixa al descobert una manca a l'expressivitat del llenguatge. És el que es coneix com la *compilació humana*, on el programador fa el que hauria de fer el compilador per a ell. Paul Graham―el fundador de YCombinator―en parla molt d'això. Més informació a [Paul Graham: Revenge of the nerds](http://www.paulgraham.com/icad.html).

3. **Utilitza el fragment de codi que mostra el llistat dels algoritmes disponibles per a les classes: KeyPairGenerator, Cifer, MessageDigest i Signature. Fes un resum dels algoritmes més bàsics (amb nom curt).**

No sé exactament a quin fragment de codi et refereixes. Aquest enunciat es refereix a informació externa. Soposo que ho vas dir el divendres, a la classe que no vaig atendre perquè no em va arribar el mail amb el link fins més tard.

4. **En el codi del github, fes les modificacions següents sobre l’original i contesta a les següents preguntes**

    - **Amb el missatge que hi ha, quina és la longitud de clau més petita que pots fer anar?**
    
        Segons [aquest apartat](https://docs.oracle.com/javase/specs/jls/se15/html/jls-4.html#jls-4.3.3) de l'especificació del llenguatge, una instància de la classe `String` representa una seqüència de punts de codi. A la meva plataforma, la codificació dels literals de tipus String és la "UTF-8", i és la que farem servir per a calcular la mida en bytes del missatge.

        [L'Script de jshell per trobar la mida del missatge el podem trobar aquí](./java/src/scripts/calcularBytes.java). Està escrit per ser executat en Linux. El resultat és aquest:

        ![calcularBytes-result]

        La mida màxima del missatge en bits `m`fent servir l'estil PKCS#1 "old-style" fent servir una clau de mida `n bits` és de `m = n/8 - 11`. Per tant, la mida mínima en bits de la clau serà de `n = (m + 11)* 8`. Per a un missatge de 90 bytes es necessita una clau de 101 bytes.

    - **Correspon el límit teòric de la fòrmula de longitud de clau RSA dels apunts? (comprar la fórmula amb la realitat de java)**
        Al executar el programa, el límit indicat per a una clau de 1024 bits és de 117 bytes. Aplicant la fòrmula `m = n/8 - 11`, aquesta mida coincideix amb els límits teòrics.

    - **Comprova que generar claus més grans comporta més temps d’encriptació. Mesura el temps en java.**
    
        [El codi el podem trobar aquí.](./java/src/main/java/example/Exemple02RSA.java)
    
        Podem comprobar a les següents imatges que el temps augmenta molt més ràpid per a la encriptació (ordres de magnitud) que per a la desencriptació (aproximadament el doble per a una clau quatre cops més llarga). 

        ![short-key-time]

        ![long-key-time]  

    - **Canvia el codi per a utilitzar el missatge xifrat i desxifrat en Base64.**
    
        [El codi és a aquest arxiu](./java/src/main/java/main/Exemple02RSA.java).
        
        El resultat d'executar-lo és a la següent imatge:
        
        ![base64] 

[fac-pattern]: ./latex/images/factory-pattern.jpg "El patró fàbrica."
[calcularBytes-result]: ./latex/images/calcularBytes-result.png "Calculant els bytes del missatge."
[short-key-time]: ./latex/images/short-key-time.png "Temps per encriptar i desencriptar amb una clau curta."
[long-key-time]: ./latex/images/long-key-time.png "Temps per encriptar i desencriptar amb una clau llarga."
[base64]: ./latex/images/base64.png "Encriptat i desencriptat en base 64."
