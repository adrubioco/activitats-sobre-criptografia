package main;

import example.*;
import javax.crypto.Cipher;
import java.security.*;
import java.util.Base64;
import java.util.Set;

public class Exemple02RSA {
  
  public static void main(String[] args)throws Exception {
    String missatge = "Hola mon. Això es un missatge de prova llarg. I amb aquesta frase l'allarguem encar més.";
    int keySize = 2024;
    KeyPairGenerator keyPairFactory = KeyPairGenerator.getInstance("RSA");
    keyPairFactory.initialize(keySize); // >1024 strong encription
    
    KeyPair keys = keyPairFactory.generateKeyPair();
    PrivateKey privateKey = keys.getPrivate();
    PublicKey publicKey = keys.getPublic();
    
    Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.ENCRYPT_MODE, privateKey);
    
    long start = System.nanoTime();
    String encryptedMessage = Base64.getEncoder().encodeToString(cipher.doFinal(missatge.getBytes()));
    //byte[] encryptedMessage = cipher.doFinal(missatge.getBytes());
    long end = System.nanoTime();
    //System.out.println(hex(encryptedMessage));
    System.out.println(encryptedMessage);
    System.out.println("Encrypted with key of " + keySize + " bytes: " + (end - start) + " nanoseconds");
    
    //Desencriptar
    
    Cipher decipher = Cipher.getInstance("RSA");
    decipher.init(Cipher.DECRYPT_MODE, publicKey);
    
    start = System.nanoTime();
    String decryptedMessage = new String(decipher.doFinal(Base64.getDecoder().decode(encryptedMessage)));
    //String decryptedMessage = new String(decipher.doFinal(encryptedMessage));
    end = System.nanoTime();
    System.out.println("Decrypted with key of " + keySize + " bytes: " +  (end - start) + " nanoseconds");
    System.out.println(decryptedMessage);
    
  }
  
  private static String hex(byte[] bytes) {
    StringBuilder sb = new StringBuilder();
    for (int i=0; i<bytes.length; i++) {
      sb.append(String.format("%02X ",bytes[i]));
    }
    return sb.toString();
  }
}